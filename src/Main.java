import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Get User input
        Scanner inputNumberOfDices = new Scanner(System.in);
        System.out.println("Enter the number of dices you want to throw:");
        int numberOfDices = inputNumberOfDices.nextInt();
        System.out.println(numberOfDices + " dice will be thrown");

        for (int i = 1; i <= numberOfDices; i++){
            DiceThread diceThread = new DiceThread("Thread " + i);
            diceThread.start();
        }

        //System.out.println("The results are: ");
    }
}
