package RaceConditionAfter;

public class RaceConditionSolved implements Runnable {

    int stock = 1;

    public static void main(String[] args) {
        RaceConditionSolved raceCondition = new RaceConditionSolved();
        Thread thread1 = new Thread(raceCondition, "Thread 1");
        Thread thread2 = new Thread(raceCondition, "Thread 2");
        thread1.start();
        thread2.start();
    }

    @Override
    public void run() {
        //Only one thread can be executed
        synchronized (this) {//solving the race condition by placing a lock on the instance
            if (stock > 0) {
                try {
                    System.out.println(Thread.currentThread().getName());
                    Thread.sleep(1000);
                } catch (InterruptedException exception) {
                    System.out.println(Thread.currentThread().getName() + " has been interrupted");
                }
                stock--;
                System.out.println(": " + Thread.currentThread().getName() + " amount of stock left: " + stock);
            } else {
                System.out.println("We are out of stock");
            }
        }
    }
}