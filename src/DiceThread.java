import java.util.Random;

public class DiceThread extends Thread {

    private Thread thread;
    private String DiceId;

    DiceThread( String diceName){
        DiceId = diceName;
        System.out.println("Making " + DiceId);
    }

    public void run(){
        System.out.println("Running " + DiceId);
        try {
            //create normal dice with 6 sides
            System.out.println("Throw the dice!");
            int numberOfSides = 6;
            int roll = (int) (Math.random() * numberOfSides) + 1;
            System.out.println("Result: " + roll);
            //let the thread sleep for a short moment
            Thread.sleep(100);
        } catch (InterruptedException exception){
            System.out.println("Dice " + DiceId + " has been interrupted");
        }
        System.out.println("Throwing dice " + DiceId + " has finished");
    }

    public void start (){
        System.out.println("Start throwing dice " + DiceId + " on it's own thread");
        if(thread == null){
            thread = new Thread(this, DiceId);
            thread.start();
        }
    }
}
